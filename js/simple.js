
let txt = "I am a CSIT final year student. Currently, i'm enrollled as a candidate for WICC (Winter Code Camp) training program in Python domain. This code camp is really a great opportunity for students like me to gain real world experience and make connections with other senior developers.";
var i = 0;    
var speed = 40;

document.addEventListener("DOMContentLoaded", typeWriter);

/*
var opacity = 50;
function opacityImage(){
    if(opacity < 100){
        document.getElementById("first-image-id").style.opacity = opacity+"%";
        opacity++;
        setTimeout(opacityImage, speed);
    } 
    typeWriter()
}*/

function typeWriter(){ 

    if (i < txt.length){
        document.getElementById("second-text-id").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}